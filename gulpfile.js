const gulp = require('gulp');
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const runSequence = require('run-sequence');
const path = require('path');

const srcDir = path.join(__dirname, 'src');
const assetsDir = path.join(__dirname, 'assets');
const distDir = path.join(__dirname, 'dist');

gulp.task('default', (done) => {
  return runSequence('clean', ['pug', 'files', 'sass', 'assets'], done);
});

gulp.task('watch', () => {
  return gulp.watch([srcDir + '/**/*.*', assetsDir + '/**/*.*'], ['default']);
});

gulp.task('pug', () => {
  return gulp.src([
    srcDir + '/*.pug',
    '!' + srcDir + '/includes/*.*'
  ])
    .pipe(pug({ pretty: true }))
    .pipe(gulp.dest(distDir));
});

gulp.task('files', () => {
  return gulp.src([
    srcDir + '/**/*.*',
    '!**/*.pug',
    "!**/*.sass"
  ])
    .pipe(gulp.dest(distDir));
});

gulp.task('sass', () => {
  return gulp.src(srcDir + '/*.sass')
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(autoprefixer({
            browsers: ['last 10 versions'],
            cascade: false
        }))
    .pipe(gulp.dest(distDir));
});

gulp.task('assets', () => {
  return gulp.src(assetsDir + '/**/*.*')
    .pipe(gulp.dest(distDir));
});

gulp.task('clean', () => {
  return del([distDir + '/**', '!' + distDir]);
});
